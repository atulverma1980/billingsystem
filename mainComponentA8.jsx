import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import ShowCartA8 from "./showCartA8";
import ShowTableA8 from "./showTableA8";
class MainComponentA8 extends Component{
    state={
     product : [
            {"code":"PEP221","prod":"Pepsi","price":12,"instock":"Yes","category":"Beverages"},
            {"code":"COK113","prod":"Coca	Cola","price":18,"instock":"Yes","category":"Beverages"},
            {"code":"MIR646","prod":"Mirinda","price":15,"instock":"No","category":"Beverages"},
            {"code":"SLI874","prod":"Slice","price":22,"instock":"Yes","category":"Beverages"},
            {"code":"MIN654","prod":"Minute	Maid","price":25,"instock":"Yes","category":"Beverages"},
            {"code":"APP652","prod":"Appy","price":10,"instock":"No","category":"Beverages"},
            {"code":"FRO085","prod":"Frooti","price":30,"instock":"Yes","category":"Beverages"},
            {"code":"REA546","prod":"Real","price":24,"instock":"No","category":"Beverages"},
            {"code":"DM5461","prod":"Dairy	Milk","price":40,"instock":"Yes","category":"Chocolates"},
            {"code":"KK6546","prod":"Kitkat","price":15,"instock":"Yes","category":"Chocolates"},
            {"code":"PER5436","prod":"Perk","price":8,"instock":"No","category":"Chocolates"},
            {"code":"FST241","prod":"5	Star","price":25,"instock":"Yes","category":"Chocolates"},
            {"code":"NUT553","prod":"Nutties","price":18,"instock":"Yes","category":"Chocolates"},
            {"code":"GEM006","prod":"Gems","price":8,"instock":"No","category":"Chocolates"},
            {"code":"GD2991","prod":"Good	Day","price":25,"instock":"Yes","category":"Biscuits"},
            {"code":"PAG542","prod":"Parle	G","price":5,"instock":"Yes","category":"Biscuits"},
            {"code":"MON119","prod":"Monaco","price":7,"instock":"No","category":"Biscuits"},
            {"code":"BOU291","prod":"Bourbon","price":22,"instock":"Yes","category":"Biscuits"},
            {"code":"MAR951","prod":"MarieGold","price":15,"instock":"Yes","category":"Biscuits"},
            {"code":"ORE188","prod":"Oreo","price":30,"instock":"No","category":"Biscuits"}
            ],
            pr:{
                category:"",
                price:"",
                stock:"",
            },
            
             arrCat : ['Beverages','Chocolates','Biscuits'],
             arrStock:['Yes','No'],
             arrPrice:['<10','10-20','>20'],
             cart:[],
             view:0,
             filter:[],
             arr:[],
             arr1:[],
             arr2:[],
             val:-1,
    };
    filterByCategory=()=>{
        let s1 = {...this.state};
        s1.filter = s1.product.filter(v=>v.category==s1.pr.category);
        s1.arr=s1.filter;
        this.setState(s1);
    }
    filterByStock=()=>{
        let s1 = {...this.state};
        if(s1.pr.stock==""){
            s1.filter=s1.arr;
        }else{
        s1.filter = s1.arr.filter(v=>v.instock==s1.pr.stock);
        }
        s1.arr2 = s1.filter;
        this.setState(s1);
    }
    filterByPrice=()=>{
        let s1 = {...this.state};
        if(s1.pr.price==""){
            s1.filter=s1.arr2;
        }else{
        s1.filter = s1.arr2.filter(val=>{
            if(s1.pr.price=='<10'){
                return val.price<10;
            }else if(s1.pr.price=='10-20'){
                return (val.price>=10 && val.price<20);
            }else{
                return val.price>20;
            }
        });
    }
        this.setState(s1);
    }
    sort=(colNo)=>{
        let s1 = {...this.state};
        switch(colNo){
            case 0:s1.filter.length==0?s1.product.sort((p1,p2)=>p1.code.localeCompare(p2.code)):s1.filter.sort((p1,p2)=>p1.code.localeCompare(p2.code)); break;
            case 3:s1.filter.length==0?s1.product.sort((p1,p2)=>(+p1.price)-(+p2.price)):s1.filter.sort((p1,p2)=>(+p1.price)-(+p2.price)); break;
            case 1:s1.filter.length==0?s1.product.sort((p1,p2)=>p1.prod.localeCompare(p2.prod)):s1.filter.sort((p1,p2)=>p1.prod.localeCompare(p2.prod)); break;
            case 2:s1.filter.length==0?s1.product.sort((p1,p2)=>p1.category.localeCompare(p2.category)):s1.filter.sort((p1,p2)=>p1.category.localeCompare(p2.category)); break;
            case 4:s1.filter.length==0?s1.product.sort((p1,p2)=>p1.instock.localeCompare(p2.instock)):s1.filter.sort((p1,p2)=>p1.instock.localeCompare(p2.instock));break;

        }
        s1.val=colNo;
        this.setState(s1);
    }
    addToCart=(index)=>{
        let s1 = {...this.state};  
     let arr =  s1.filter.length==0?s1.product[index]:s1.filter[index];
     let jso={};
     let fnd = s1.cart.find(c=>c.code==arr.code);
     if(fnd!=undefined){
         fnd.Qty+=1;
         fnd.Amount =fnd.price*(+fnd.Qty);
     }else if(fnd==undefined){
         jso.code = arr.code;
         jso.prod=arr.prod;
         jso.price =(+arr.price);
         jso.Qty = 1;
         jso.Amount = jso.price*jso.Qty;
         s1.cart.push(jso);
     }        this.setState(s1);
    }
    add=(index)=>{
        let s1 = {...this.state};
        s1.cart[index].Qty+=1;
        s1.cart[index].Amount=s1.cart[index].price*s1.cart[index].Qty;
        this.setState(s1);
    }
    reduce=(index)=>{
        let s1 = {...this.state};
        s1.cart[index].Qty-=1;
        s1.cart[index].Amount=s1.cart[index].price*s1.cart[index].Qty;
        if(s1.cart[index].Qty<=0){
            s1.cart.splice(index,1);
        }
        this.setState(s1);
    }
    remove=(index)=>{
        let s1 = {...this.state};
        s1.cart.splice(index,1);
        this.setState(s1); 
    }
    close=()=>{
        let s1 = {...this.state};
        alert("Are you sure to close ??")
        s1.cart=[];
        this.setState(s1);
    }
    showBill=()=>{
        let s1 = {...this.state};
        s1.view=1;
        this.setState(s1);
    }
    render(){
        const{product,arrCat,arrStock,arrPrice,cart,view,filter,val}=this.state;
        const{category,price,stock}=this.state.pr;
        let arr = filter.length==0?product:filter;
        return(<React.Fragment>
            <div className="container-fluid">
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
                <a className="navbar-brand text-white" href="#" >
                    BillingSystem
                </a>
              <div className="" id="navbarSupportedContent">
                  <ul className="navbar-nav ">
                      <li className="nav-item ">
                          <a className="nav-link" href="#">
                              <span onClick={()=>this.showBill()}>
                                  New Bill
                              </span>
                          </a>
                      </li>
                  </ul>
              </div>  
            </nav> 
            {view==1?(
            <ShowCartA8 cart={cart} add={this.add} reduce={this.reduce} remove={this.remove} close={this.close}/>):""}
            {view==1?(<React.Fragment>
                <h3 className="text-center">Product List</h3>
            <div className="row">
                <div className="col-2"><h5>Filter Products By:-</h5></div>
                <div className="col-3" onChange={()=>this.filterByCategory()}>{this.makeDD("Select Category",arrCat,"category",category)}</div>
                <div className="col-3" onChange={()=>this.filterByStock()}>{this.makeDD("Select in Stock",arrStock,"stock",stock)}</div>
                <div className="col-3" onChange={()=>this.filterByPrice()}>{this.makeDD("Select Price Range",arrPrice,"price",price)}</div>
            </div>
            <div className="row text-center border my-2 bg-dark text-white">
                <div className="col-2 border"onClick={()=>this.sort(0)}>{val==0?"Code(X)":"Code"}</div>
                <div className="col-2 border"onClick={()=>this.sort(1)}>{val==1?"Product(X)":"Product"}</div>
                <div className="col-2 border"onClick={()=>this.sort(2)}>{val==2?"Category(X)":"Category"}</div>
                <div className="col-2 border"onClick={()=>this.sort(3)}>{val==3?"Price(X)":"Price"}</div>
                <div className="col-2 border"onClick={()=>this.sort(4)}>{val==4?"InStock(X)":"InStock"}</div>
                <div className="col-2 border"></div>
            </div>
            {arr.map((v,index)=><ShowTableA8 data={v} index={index} onAdd={this.addToCart}/>)}
            </React.Fragment>):""}
            </div>
        </React.Fragment>);
    }
    makeDD=(header,arr,name,value)=>{
        return (
        <select className="form-control" name={name} value={value} onChange={this.handleChange}>
            <option value="">{header}</option>
            {arr.map(v=><option>{v}</option>)}
        </select>
        );
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.pr[input.name]=input.value;
        this.setState(s1);
    }
}
export default MainComponentA8;