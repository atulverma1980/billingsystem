import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
class ShowCartA8 extends Component{
    render(){
        const{cart,add,remove,close,reduce}=this.props;
        return (<React.Fragment>
            <div className="caraousal-fluid bg-light my-4">
                <h3>Detail of Current Bill</h3>
                {cart.length==0?(
                    "Items = 0,Quantity=0,Amount=0"
                ):(<React.Fragment>
                    Items={cart.reduce((acc,curr)=>acc+1,0)}
                    Quantity= {cart.reduce((acc,curr)=>acc+curr.Qty,0)}
                    Amount={cart.reduce((acc,curr)=>acc+curr.Amount,0)}<br/>
                        {cart.map((v,index)=>{
                            return(
                                <div className="col-6">
                                    Code={v.code},
                                    Name={v.prod},
                                    Price={v.price},
                                    Qty={v.Qty},
                                    Amount={v.Amount}
                                    <button className="btn btn-success m-1"onClick={()=>add(index)}>+</button>
                                    <button className="btn btn-warning m-1"onClick={()=>reduce(index)}>-</button>
                                    <button className="btn btn-danger m-1"onClick={()=>remove(index)}>x</button>
                                </div>
                            );
                        })}
                        <button className="btn btn-primary"onClick={()=>close()}>Close</button>
                        
                    
                </React.Fragment>)}
            </div>
        </React.Fragment>)
    }
}
export default ShowCartA8;