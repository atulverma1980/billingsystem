import React,{Component} from "react";
import "bootstrap/dist/css/bootstrap.min.css";
class ShowTableA8 extends Component{
    render(){
        const{data,index,onAdd}=this.props;
        return (<React.Fragment>
            <div className="row text-center border">
            <div className="col-2 border">{data.code}</div>
                <div className="col-2 border">{data.prod}</div>
                <div className="col-2 border">{data.category}</div>
                <div className="col-2 border">{data.price}</div>
                <div className="col-2 border">{data.instock}</div>
                <div className="col-2 border">
                    <button className="btn btn-success m-1" onClick={()=>onAdd(index)}>Add to Cart</button>
                </div>
            </div>

        </React.Fragment>);
    }
}
export default ShowTableA8;